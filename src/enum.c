#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"

char * getNameOperator(enum OPERATOR x){
    switch(x){
        case OP_INITF : return "INIC_FUN";
        case OP_ENDF  : return "END_FUN";
        case OP_SUMA  : return "SUMA";
        case OP_RESTA : return "RESTA";
        case OP_MULTI : return "MULT";
        case OP_DIV   : return "DIV";
        case OP_MOD   : return "MOD";
        case OP_MEN   : return "MENOR";
        case OP_MAY   : return "MAYOR";
        case OP_URES  : return "NEG";
        case OP_NEG   : return "NO";
        case OP_AND   : return "AND";
        case OP_OR    : return "OR";
        case OP_EQ    : return "EQUALS";
        case OP_PAR   : return "PARENTESIS";
        case OP_ASIGN : return "ASIGN";
        case OP_CALLF : return "CALL";
        case OP_PUSHP : return "PUSH";
        case OP_IFFALSE : return "IF_FALSE";
        case OP_LABEL : return "LABEL";
        case OP_JMP   : return "JMP";
        case OP_RETURN: return "RETURN";
        case OP_GLOBAL_VAR: return "VAR";
        case OP_VAR_INIC: return "VAR_DECL_INIC";
        case OP_VAR_END : return "VAR_DECL_END";
        case OP_LOADP : return "LOAD";
        case OP_EXTERN: return "EXTERN";
        
    }

}
