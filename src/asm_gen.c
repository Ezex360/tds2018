#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"

extern char * filename;
int loadp; //parametros formales
int cant_params; //cantidad total de parametros

char * asm_var_reg(variable * var){
	char * aux = (char*) malloc(sizeof(char[16]));
	if(var->scope)//es global
		sprintf(aux,"%s(%%rip)",var->name);
	else if(var->offset == 0)
		sprintf(aux,"$%s",var->value);
	else
		sprintf(aux,"-%d(%%rbp)",var->offset);
	return aux;
}

void asm_fun_begin(instruction * code,FILE *file){
	loadp = 1;
	char * fun_name = code->arg0->name;
	cant_params = code->arg0->scope;
	fprintf(file,".global %s\n",fun_name);
	fprintf(file,".type %s, @function\n",fun_name);
	fprintf(file,"%s:\n",fun_name);
	fprintf(file,"\tpushq %%rbp\n");
	fprintf(file,"\tmovq %%rsp, %%rbp\n");
	if(code->arg0->offset > 0)
		fprintf(file,"\tsubq $%d, %%rsp\n",code->arg0->offset);
	fprintf(file,"\n");
}

void asm_fun_end(instruction * code,FILE *file){
	fprintf(file,"\n\tleave\n");
	fprintf(file,"\tret\n");
}

void asm_global_var(instruction * code,FILE *file){
	fprintf(file,".comm %s,8,8\n",code->arg0->name);
}


void asm_fun_asign(instruction * code,FILE *file){
	char * var0,* var1;
	var0 = asm_var_reg(code->arg0);
	var1 = asm_var_reg(code->arg1);
	if(code->arg1->offset==0)
		fprintf(file,"\tmovl $%s, %s\n",code->arg1->value,var0);
	else{
		fprintf(file,"\tmovl %s, %%eax\n",var1);
		fprintf(file,"\tmovl %%eax, %s\n",var0);
	}
}

void asm_fun_operator(instruction * code,FILE *file){
	char * var0,* var1, * res;
	var0 = asm_var_reg(code->arg0);
	if(code->arg1)
		var1 = asm_var_reg(code->arg1);
	if(code->res)
		res = asm_var_reg(code->res);
	char *L1,*L2,*L3;
	fprintf(file,"\tmovl %s, %%eax\n",var0);
	switch(code->op){
		case OP_SUMA  :	fprintf(file,"\taddl %s, %%eax\n",var1); break;
		case OP_RESTA : fprintf(file,"\tsubl %s, %%eax\n",var1); break;
		case OP_MULTI : fprintf(file,"\timull %s, %%eax\n",var1); break;
		case OP_DIV   : fprintf(file,"\tcltd\n\tmovl %s, %%ebx\n\tidivl %%ebx\n",var1); break;
		case OP_MOD   : fprintf(file,"\tcltd\n\tmovl %s, %%ebx\n\tidivl %%ebx\n",var1); break;
		case OP_URES  : fprintf(file,"\tnegl %%eax\n"); break;
		case OP_MEN   : fprintf(file,"\tcmpl %s, %%eax\n",var1);
						fprintf(file,"\tsetl %%al\n\tmovzbl %%al, %%eax\n"); break;
		case OP_MAY   : fprintf(file,"\tcmpl %s, %%eax\n",var1);
						fprintf(file,"\tsetg %%al\n\tmovzbl %%al, %%eax\n"); break;
		case OP_NEG   : fprintf(file,"\tcmpl $0, %%eax\n");
						fprintf(file,"\tsete %%al\n\tmovzbl %%al, %%eax\n"); break;
		case OP_EQ    : fprintf(file,"\tcmpl %s, %%eax\n",var1);
						fprintf(file,"\tsete %%al\n\tmovzbl %%al, %%eax\n"); break;
		case OP_AND   : L1 = new_label(); L2 = new_label();
						fprintf(file,"\tcmpl $0, %%eax\n");
						fprintf(file,"\tje .%s\n",L1);
						if(code->arg1->offset==0)
							fprintf(file,"\tmovl %s,%%eax\n\tcmpl $0,%%eax\n",var1);
						else
							fprintf(file,"\tcmpl $0,%s\n",var1);
						fprintf(file,"\tje .%s\n\tmovl $1, %%eax\n\tjmp .%s\n",L1,L2);
						fprintf(file,".%s:\n\tmovl $0, %%eax\n.%s:\n",L1,L2); break;
		case OP_OR    : L1 = new_label(); L2 = new_label(); L3 = new_label();
						fprintf(file,"\tcmpl $0, %%eax\n");
						fprintf(file,"\tjne .%s\n",L1);
						if(code->arg1->offset==0)
							fprintf(file,"\tmovl %s,%%eax\n\tcmpl $0,%%eax\n",var1);
						else
							fprintf(file,"\tcmpl $0,%s\n",var1);
						fprintf(file,"\tje .%s\n.%s:\n\tmovl $1, %%eax\n\tjmp .%s\n",L2,L1,L3);
						fprintf(file,".%s:\n\tmovl $0, %%eax\n.%s:\n",L2,L3); break;
		case OP_PAR   : fprintf(file,"\t#PARENTESIS\n"); break;
	}
	if(code->op != OP_MOD)
		fprintf(file,"\tmovl %%eax, %s\n",res);
	else
		fprintf(file,"\tmovl %%edx, %s\n",res);

}
void asm_if(instruction *code,FILE *file){
	char * var0,* var1;
	var0 = asm_var_reg(code->arg0);
	if(code->arg0->offset==0)
		fprintf(file,"\tmovl %s,%%eax\n\tcmpl $0,%%eax\n",var0);
	else
		fprintf(file,"\tcmpl $0, %s\n",var0);
	fprintf(file,"\tje .%s\n",code->arg1->name);
}

void asm_label(instruction *code,FILE *file){
	fprintf(file, ".%s:\n",code->arg0->name );
}

void asm_jmp(instruction *code,FILE *file){
	fprintf(file, "\tjmp .%s\n",code->arg0->name );	
}


void asm_fun_loadp(instruction * code,FILE *file){
	char * var0;
	var0 = asm_var_reg(code->arg0);
	switch(loadp){
		case 1: fprintf(file, "\tmovl %%edi, %s\n",var0); break;
		case 2: fprintf(file, "\tmovl %%esi, %s\n",var0); break;
		case 3: fprintf(file, "\tmovl %%edx, %s\n",var0); break;
		case 4: fprintf(file, "\tmovl %%ecx, %s\n",var0); break;
		case 5: fprintf(file, "\tmovl %%r8d, %s\n",var0); break;
		case 6: fprintf(file, "\tmovl %%r9d, %s\n",var0); break;
		default:fprintf(file, "\tmovl %d(%%rbp),%%eax\n\tmovl %%eax, %s\n",16+(8*(cant_params - loadp)),var0); break;
	}
	loadp++;
}

void asm_pushp(instruction *code,FILE *file,int nparam){
	char * var0;
	var0 = asm_var_reg(code->arg0);
	switch(nparam){
		case 1: fprintf(file, "\tmovl %s, %%edi\n",var0); break;
		case 2: fprintf(file, "\tmovl %s, %%esi\n",var0); break;
		case 3: fprintf(file, "\tmovl %s, %%edx\n",var0); break;
		case 4: fprintf(file, "\tmovl %s, %%ecx\n",var0); break;
		case 5: fprintf(file, "\tmovl %s, %%r8d\n",var0); break;
		case 6: fprintf(file, "\tmovl %s, %%r9d\n",var0); break;
		case 7: fprintf(file, "\tpush %s\n",var0); break;
		default: fprintf(file, "\tpush %s\n",var0); break;
	}
}

void asm_call_fun(instruction *code,FILE *file){
	char * var1;
	fprintf(file, "\tmovl $0,%%eax\n\tcall %s\n",code->arg0->name);
	if(code->arg1){
		var1 = asm_var_reg(code->arg1);
		fprintf(file, "\tmovl %%eax, %s\n",var1);
	}
}

void asm_fun_ret(instruction *code,FILE *file){
	char * var0;
	if(code->arg0){
		var0 = asm_var_reg(code->arg0);
		fprintf(file, "\tmovl %s,%%eax\n",var0 );
	}
}

char * get_fname(){
	char *ch;
	char * final="";
	ch = strtok(filename, "/");
	while (ch != NULL) {
	final = ch;
	ch = strtok(NULL, "/");
	}
	final = strtok(final,".");
	strcat(final, ".s");
	return final;
}


void asm_generator(instruction * codigo){
	int nparam = 1; //parametros actuales
	//int line=1;
	instruction * code = codigo;
	FILE *file = NULL;
	char * fname = get_fname();
	file = fopen(fname, "w");
	while(code){
		//printf("%d %s\n",line++,getNameOperator(code->op));
		switch(code->op){
			//case OP_VAR_INIC  : fprintf(file,".STR:\n\t.string \"%%d\\n\"\n"); break;
			case OP_GLOBAL_VAR: asm_global_var(code,file); break;
			case OP_VAR_END   : fprintf(file,".text\n"); break;
			case OP_LOADP : asm_fun_loadp(code,file); break;
			case OP_INITF : asm_fun_begin(code,file); break;
			case OP_ENDF  : asm_fun_end(code,file); break;
			case OP_ASIGN : asm_fun_asign(code,file); break;
			case OP_SUMA  :	asm_fun_operator(code,file); break;
			case OP_RESTA : asm_fun_operator(code,file); break;
			case OP_MULTI : asm_fun_operator(code,file); break;
			case OP_DIV   : asm_fun_operator(code,file); break;
			case OP_MOD   : asm_fun_operator(code,file); break;
			case OP_MEN   : asm_fun_operator(code,file); break;
			case OP_URES  : asm_fun_operator(code,file); break;
			case OP_NEG   : asm_fun_operator(code,file); break;
			case OP_AND   : asm_fun_operator(code,file); break;
			case OP_OR    : asm_fun_operator(code,file); break;
			case OP_MAY   : asm_fun_operator(code,file); break;
			case OP_EQ    : asm_fun_operator(code,file); break;
			case OP_PAR   : asm_fun_operator(code,file); break;
			case OP_IFFALSE : asm_if(code,file); break;
			case OP_LABEL : asm_label(code,file); break;
			case OP_JMP   : asm_jmp(code,file); break;
			case OP_PUSHP : asm_pushp(code,file,nparam);nparam++; break;
			case OP_CALLF : nparam = 1; asm_call_fun(code,file); break;
			case OP_RETURN : asm_fun_ret(code,file); break;

	       
	    }
		code = code->next;
	}
	 fclose(file);
}
