#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"

int temp = 0;
int label = 0;

extern int parentesis(node * n);
extern int rel_op(node * n);
extern int math_op(node * n);
extern int checkNodeInt(node * n);
extern int checkNodeBool(node * n);


char * new_id();

int is_operator(node * n){
	return parentesis(n) || rel_op(n) || math_op(n);
}

char * new_label(){
	char * t = malloc(sizeof(char[5]));
	sprintf(t, "%d", label);
	label++;
	char s[6] = "L";
	strcat(s,t);
	strcpy(t,s);
	return t;
}

char * new_id(){
	char * t = malloc(sizeof(char[5]));
	sprintf(t, "%d", temp);
	temp++;
	char s[6] = "T";
	strcat(s,t);
	strcpy(t,s);
	return t;
}

char * ret_id(int temp){
	char * t = malloc(sizeof(char[5]));
	sprintf(t, "%d", temp);
	char s[6] = "T";
	strcat(s,t);
	strcpy(t,s);
	return t;
}

void * print_ins(instruction * code){
	while(code){
		printf("%s ",getNameOperator(code->op));
		if(code->arg0){
			printf("%s ",code->arg0->name);
		}
		if(code->arg1){
			printf("%s ",code->arg1->name);
		}
		if(code->res){
			printf("%s",code->res->name);
		}
		printf("\n");
		code = code->next;
	}
}

instruction * new_instruction(enum OPERATOR op,variable * arg0,variable * arg1,variable * res){
	instruction * ret = (instruction*) malloc(sizeof(instruction));
	ret->op = op;
	ret->arg0 = arg0;
	ret->arg1 = arg1;
	ret->res = res;
	ret->next = NULL;
	return ret;
}

instruction * add_instuction(instruction * base, instruction * add){
	if(base){
		instruction * aux = base;
		while(aux->next){
			aux = aux->next;
		}
		aux->next = add;
	}else{
		base = add;
	}
	return base;
}



variable * expr_generator(node * nodo,instruction * code,int offset){
	variable * ret,* arg0,* arg1; 
	if(is_operator(nodo)){
		arg0 = expr_generator(nodo->child,code,offset);
		if(nodo->child->next){
			if(arg0->offset > offset)
				offset=arg0->offset;
			arg1 = expr_generator(nodo->child->next,code,offset);
			if(arg1->offset > offset)
				offset=arg1->offset;
			ret = new_var("resultado",new_id(),NULL,0,offset+8);
		}else{
			arg1 = NULL;
			if(arg0->offset > offset)
				offset=arg0->offset;
			ret = new_var("resultado",new_id(),NULL,0,offset+8);
		}
		add_instuction(code,new_instruction(nodo->op,arg0,arg1,ret));
	}else if(nodo->fun){
		node * aux;
		aux=nodo->child->child;
		node * push_list = NULL;
			if(aux){
				arg0 = expr_generator(aux,code,offset);
				push_list = new_node_var(arg0);
				if(arg0->offset > offset)
					offset=arg0->offset;
				aux=aux->next;
			}
			while(aux){
				arg0 = expr_generator(aux,code,offset);
				if(arg0->offset > offset)
					offset=arg0->offset;
				add_sibling(push_list,new_node_var(arg0));
				aux = aux->next;
			}
			while(push_list){
				//printf("%s\n",push_list->var->name );
				add_instuction(code,new_instruction(OP_PUSHP,push_list->var,NULL,NULL));
				push_list = push_list->next;
			}
		arg0 = new_var("fun",nodo->fun->name,nodo->fun->returnn,0,0);
		if(strcmp(nodo->fun->returnn,"VOID")){
			ret = new_var("resultado",new_id(),NULL,0,offset+8);
			add_instuction(code,new_instruction(OP_CALLF,arg0,ret,NULL));
		}else
			add_instuction(code,new_instruction(OP_CALLF,arg0,NULL,NULL));
	}else if(nodo->var){
		ret = nodo->var;
	}else{
		if(checkNodeInt(nodo)){
			ret = new_var("integer",nodo->data,nodo->data,0,0);
			//ret = new_var("integer",new_id(),nodo->data,0,offset+8);
			//add_instuction(code,new_instruction(OP_ASIGN,ret,arg0,NULL));
		}
		else if(checkNodeBool(nodo)){
			char * boolean;
			if(!strcmp(nodo->data,"FALSE"))
				boolean = "0";
			else
				boolean = "1";
			ret = new_var("bool",boolean,boolean,0,0);
			//ret = new_var("bool",new_id(),boolean,0,offset+8);
			//add_instuction(code,new_instruction(OP_ASIGN,ret,arg0,NULL));
		}
	}
	return ret;

}

int fun_generator(node * nodo,instruction * code,int offset){

	node * aux;
	variable * arg0,* arg1, * res;
	int nerrors = 0;
	while(nodo){
		if(!strcmp(nodo->data,"=")){
			aux = nodo->child;
			arg0 = aux->var;
			arg1 = expr_generator(aux->next,code,offset);
			if(arg1->offset > offset)
				offset=arg1->offset;
			instruction * arg_aux = new_instruction(OP_ASIGN,arg0,arg1,NULL);
			add_instuction(code,arg_aux);
		}else if(!strcmp(nodo->data,"if")){
			aux = nodo->child;
			arg0 = expr_generator(aux,code,offset);
			offset = arg0->offset;
			arg1 = new_var("label",new_label(),"if_false",0,0);
			res = new_var("label",new_label(),"if_true",0,0);
			add_instuction(code,new_instruction(OP_IFFALSE,arg0,arg1,NULL));
			if(aux->next){
				offset = fun_generator(aux->next->child,code,offset);
				if(aux->next->next){
					add_instuction(code,new_instruction(OP_JMP,res,NULL,NULL));
				}
			}
			add_instuction(code,new_instruction(OP_LABEL,arg1,NULL,NULL));
			if(aux->next && aux->next->next){
				offset = fun_generator(aux->next->next->child,code,offset);
				add_instuction(code,new_instruction(OP_LABEL,res,NULL,NULL));
			}
		}else if(!strcmp(nodo->data,"while")){
			arg0 = new_var("label",new_label(),"if_true",0,0);
			arg1 = new_var("label",new_label(),"if_false",0,0);
			add_instuction(code,new_instruction(OP_LABEL,arg0,NULL,NULL));
			res = expr_generator(nodo->child,code,offset);
			offset = res->offset;
			add_instuction(code,new_instruction(OP_IFFALSE,res,arg1,NULL));
			offset = fun_generator(nodo->child->next,code,offset);
			add_instuction(code,new_instruction(OP_JMP,arg0,NULL,NULL));
			add_instuction(code,new_instruction(OP_LABEL,arg1,NULL,NULL));
		}else if(!strcmp(nodo->data,"Return")){
			if(!strcmp(nodo->child->data,"VOID"))
				res = NULL;
			else{
				res = expr_generator(nodo->child,code,offset);
				if(res->offset > offset)
					offset = res->offset;
			} 
			add_instuction(code,new_instruction(OP_RETURN,res,NULL,NULL));
		}else if(!strcmp(nodo->type,"function")){
			node * aux;
			aux=nodo->child->child;
			node * push_list = NULL;
			if(aux){
				arg0 = expr_generator(aux,code,offset);
				push_list = new_node_var(arg0);
				if(arg0->offset > offset)
					offset=arg0->offset;
				aux=aux->next;
			}
			while(aux){
				arg0 = expr_generator(aux,code,offset);
				if(arg0->offset > offset)
					offset=arg0->offset;
				add_sibling(push_list,new_node_var(arg0));
				aux = aux->next;
			}
			while(push_list){
				//printf("%s\n",push_list->var->name );
				add_instuction(code,new_instruction(OP_PUSHP,push_list->var,NULL,NULL));
				push_list = push_list->next;
			}
			arg0 = new_var("fun",nodo->fun->name,nodo->fun->returnn,0,0);
			if(strcmp(nodo->fun->returnn,"VOID")){
				res = new_var("resultado",new_id(),NULL,0,offset+8);
				if(res->offset > offset)
					offset = res->offset;
				add_instuction(code,new_instruction(OP_CALLF,arg0,res,NULL));
			}else
				add_instuction(code,new_instruction(OP_CALLF,arg0,NULL,NULL));
		}
		nodo = nodo->next;
	}
	return offset;
}



instruction * inter_generator(stack * p,node * t){
	node * reco = t;
	list * pila = p->lista;
	list * params;
	int nparams;
	reco = reco->child;
	instruction * code = NULL;
	int new_offset=0;
	code = add_instuction(code,new_instruction(OP_VAR_INIC,NULL,NULL,NULL));
	while(pila){
		if(pila->var)
			code = add_instuction(code,new_instruction(OP_GLOBAL_VAR,pila->var,NULL,NULL));
		pila=pila->next;
	}
	code = add_instuction(code,new_instruction(OP_VAR_END,NULL,NULL,NULL));
	while(reco){
		params = reco->fun->params;
		nparams=0;
		while(params){
			nparams++;
			params=params->next;
		}
		variable * fun = new_var("fun",reco->fun->name,reco->fun->returnn,nparams,reco->fun->offset);
		if(!reco->child->child){
			code = add_instuction(code,new_instruction(OP_INITF,fun,NULL,NULL));
			code = add_instuction(code,new_instruction(OP_ENDF,fun,NULL,NULL));
		}else{
			if(strcmp(reco->child->child->data,"Extern")){
				code = add_instuction(code,new_instruction(OP_INITF,fun,NULL,NULL));
				params = reco->fun->params;
				while(params){
					code = add_instuction(code,new_instruction(OP_LOADP,params->var,NULL,NULL));
					params = params->next;
				}
				new_offset = fun_generator(reco->child->child,code,reco->fun->offset);
				if(new_offset % 16 != 0)
					new_offset+=8;
				fun->offset=new_offset;
				code = add_instuction(code,new_instruction(OP_ENDF,fun,NULL,NULL));
			}else
				code = add_instuction(code,new_instruction(OP_EXTERN,fun,NULL,NULL));
		}
		reco = reco->next;
	}
	return code;
}


