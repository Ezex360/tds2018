#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"

node * optimize_params(node * aux);

int is_optimized_expr(node * nodo){
	if(is_operator(nodo)){
		int exprleft = is_optimized_expr(nodo->child);
		if(nodo->child->next){
			int exprright = is_optimized_expr(nodo->child->next);
			if(exprleft == 2 && exprright == 2){
				return 0;
			}
			else if(exprleft == 0 || exprright == 0)
				return 0;
			else
				return 3;
		}else{
			if(exprleft == 2 || exprleft == 0)
				return 0;
			else
				return 3;
		}			
	}else if(nodo->var || nodo->fun){
		if(nodo->fun && nodo->child->child)
			return 0;
		return 1;
	}
	else
		return 2;
		
}

int is_optimizable(node * nodo){
	if(!strcmp(nodo->data,"="))
		return !is_optimized_expr(nodo->child->next);
	else if(!strcmp(nodo->data,"if") || !strcmp(nodo->data,"while"))
		return 1;
	else if(!strcmp(nodo->type,"function"))
		return nodo->child->child != NULL;
	else if(!strcmp(nodo->data,"Return")){
		if(strcmp(nodo->child->data,"VOID"))
			return !is_optimized_expr(nodo->child);
	}else if(is_operator(nodo))
		return !is_optimized_expr(nodo);
	return 0;
}

int boolToInt (char * data){
	return !strcmp(data,"TRUE");
}

node * eval_nodo(node * nodo){
	char * res = malloc(sizeof(char[34]));
	int r;
	switch(nodo->op){
		case OP_SUMA  : r = atoi(nodo->child->data) + atoi(nodo->child->next->data);
						sprintf(res, "%d", r);
						return new_node_char(res);
						break;
		case OP_RESTA  : r = atoi(nodo->child->data) - atoi(nodo->child->next->data);
						sprintf(res, "%d", r);
						return new_node_char(res);
						break;
		case OP_MULTI  : r = atoi(nodo->child->data) * atoi(nodo->child->next->data);
						sprintf(res, "%d", r);
						return new_node_char(res);
						break;
		case OP_DIV  : r = atoi(nodo->child->data) / atoi(nodo->child->next->data);
						sprintf(res, "%d", r);
						return new_node_char(res);
						break;
		case OP_MEN  : r = atoi(nodo->child->data) < atoi(nodo->child->next->data);
						if(r)
							return new_node_char("TRUE");
						else
							return new_node_char("FALSE");
						break;
		case OP_MAY  : r = atoi(nodo->child->data) > atoi(nodo->child->next->data);
						if(r)
							return new_node_char("TRUE");
						else
							return new_node_char("FALSE");
						break;
		case OP_EQ   : if(!strcmp(nodo->child->data,nodo->child->next->data))
							return new_node_char("TRUE");
						else
							return new_node_char("FALSE");
						break;
		case OP_AND   : r = boolToInt(nodo->child->data) && boolToInt(nodo->child->next->data);
						if(r)
							return new_node_char("TRUE");
						else
							return new_node_char("FALSE");
						break;
		case OP_OR    : r = boolToInt(nodo->child->data) || boolToInt(nodo->child->next->data);
						if(r)
							return new_node_char("TRUE");
						else
							return new_node_char("FALSE");
						break;
		case OP_MOD  : r = atoi(nodo->child->data) % atoi(nodo->child->next->data);
						sprintf(res, "%d", r);
						return new_node_char(res);
						break;
		case OP_URES  :	if(nodo->child->data[0] != '-'){
							sprintf(res, "-%s", nodo->child->data);
							return new_node_char(res);
						}else{
							int i = 0;
							while(nodo->child->data[i] != '\0')
								nodo->child->data[i] = nodo->child->data[++i];
							return nodo->child;
						}
						break;
		case OP_PAR   : return nodo->child;
						break;
		case OP_NEG   : if(!strcmp(nodo->child->data,"TRUE"))
							nodo->child->data="FALSE";
						else
							nodo->child->data="TRUE";
						return nodo->child;
						break;
	}
}

node * optimize_expr(node * nodo){
	node * left=nodo->child,*right=nodo->child->next;
	if(is_operator(nodo)){
		if(!right){
			if(left->op)
				nodo->child = optimize_expr(left);
			else if(left->fun || left->var){
				if(left->fun && left->child->child){
					left->child->child = optimize_params(left->child->child);
				}
				return left;
			}
			return eval_nodo(nodo);
		}else{
			if(left->op){
				left = optimize_expr(nodo->child);
			}
			if(right->op)
				right = optimize_expr(nodo->child->next);
			if(left->fun)
				left->child->child = optimize_params(left->child->child);
			if(right->fun)
				right->child->child = optimize_params(right->child->child);
			nodo->child = left;
			nodo->child->next = right;			
			if(left->fun || left->var || right->fun || right->var)
				return nodo;
			return eval_nodo(nodo);
		}
	}
	if(nodo->fun && nodo->child->child){
		nodo->child->child = optimize_params(nodo->child->child);
	}
	return nodo;
}

node * optimize_params(node * aux){
	node * temp;
	node * reco=aux;
	if(reco && is_optimizable(reco)){
		temp = reco->next;
		aux = optimize_expr(reco);
		reco->next = temp;
	}
	reco=reco->next;
	while(reco){
		if(is_optimizable(reco)){
			temp = reco->next;
			add_sibling(aux,optimize_expr(reco));
			reco->next = temp;
		}

		reco=reco->next;
	}
	return aux;

}

node * optimize(node * nodo){
	node * temp,*aux;
	if(!strcmp(nodo->data,"="))
		nodo->child->next = optimize_expr(nodo->child->next);
	if(!strcmp(nodo->data,"if")){
		aux = nodo->child;
		if(aux->next){
			if(aux->next->next){
				node * else_code = aux->next->next->child;
				while(else_code){
					if(is_optimizable(else_code))
						else_code = optimize(else_code);
					else_code = else_code->next; 
				}
			}
			node * if_code = aux->next->child;
			while(if_code){
				if(is_optimizable(if_code))
					if_code = optimize(if_code);
				if_code = if_code->next; 
			}
		}
		if(is_optimizable(nodo->child)){		
			temp = nodo->child->next;
			nodo->child = optimize_expr(nodo->child);
			nodo->child->next = temp;
		}
	}
	if(!strcmp(nodo->data,"while")){
		aux = nodo->child;
		if(aux->next){
			node * while_code = aux->next;
			while(while_code){
				if(is_optimizable(while_code)){
					while_code = optimize(while_code);
				}
				while_code = while_code->next; 
			}
		}
		if(is_optimizable(nodo->child)){		
			temp = nodo->child->next;
			nodo->child = optimize_expr(nodo->child);
			nodo->child->next = temp;
		}
	}
	if(!strcmp(nodo->type,"function") && is_optimizable(nodo)){
		nodo->child->child = optimize_params(nodo->child->child);
	}
	if(!strcmp(nodo->data,"Return"))
		nodo->child = optimize_expr(nodo->child);

	return nodo;
}



node * optimize_tree(node * root){
	node * reco = root->child;
	node * fun_code;
	//printf("BEGIN OPTIMIZATION\n");
	while(reco){
		if(reco->child->child && strcmp(reco->child->child->data,"Extern")){
			//printf("%s\n-----",reco->fun->name );
			fun_code = reco->child->child;
			while(fun_code){
				if(is_optimizable(fun_code)){
					//printf("is_optimizable %s\n",fun_code->data );
					fun_code = optimize(fun_code);
				}
				fun_code = fun_code->next;
			}
		}
		reco = reco->next;
	}
	//printf("END OPTIMIZATION\n");

	return root;
}

