#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"

extern int debug;

int checkNodeInt(node * n);
int checkNodeBool(node * n);
int isNumber(char ch);
int call_check(node * aux);

char * no_params_expected = "Error: No se esperaban parametros en la invocacion";
char * wrong_number_of_params = "Error: Cantidad de parametros erronea en la invocacion";
char * wrong_type_of_params = "Error: Tipos erroneos en la invocacion";
char * asign_error = "Error: Tipo invalido en la asignacion";
char * condition_error = "Error: Condicion no booleana";
char * expected_integer_return = "Error: Se esperaba retorno tipo Integer";
char * expected_bool_return = "Error: Se esperaba retorno tipo Bool";
char * expected_void_return = "Error: Se esperaba retorno tipo VOID";
char * wrong_operator = "Error: Operador erroneo";
char * not_initialized_vars = "Error: Variables no inicializadas";

int parentesis(node * n){
	return !strcmp(n->data,"()");
}

int rel_op(node * n) {
 return !strcmp(n->data,">") || !strcmp(n->data,"<") || !strcmp(n->data,"==") ||
 		!strcmp(n->data,"!") || !strcmp(n->data,"AND") || !strcmp(n->data,"OR");
}

int math_op(node * n) {
 return !strcmp(n->data,"+") || !strcmp(n->data,"-") || !strcmp(n->data,"*") ||
 		!strcmp(n->data,"/") || !strcmp(n->data,"%") || !strcmp(n->data,"U-");
}


//Dado un caracter devuelve 1 si es un numero o un 0 si no lo es
int isNumber(char ch){
	return ('0' <= ch && ch <= '9') || (ch == '-');
}

int check_error(char * msg,int line,int errors){
	printf("%s. Line: %d\n",msg,line);
	return ++errors;
}

int checkNodeInt(node * n){
	if(n->var != NULL){
		return !strcmp(n->var->type,"Integer");
	}
	else if(n->fun != NULL){
		if(call_check(n)>0)
			return 0;
		if(!strcmp(n->fun->returnn,"Integer"))
			return 1;
	}
  	else if(n->data != NULL){
	  	if(rel_op(n)){
	  		//check_error(wrong_operator,n->line,0);
	  		return 0;
	  	}
	  	else if(math_op(n) || parentesis(n)){
	  		if(!strcmp(n->data,"U-") || parentesis(n))
	  			return checkNodeInt(n->child);
	  		else
	  			return checkNodeInt(n->child) && checkNodeInt(n->child->next);
	   	}
	    else if(isNumber(n->data[0]))
	    	return 1;
	}
	return 0;
}

int checkNodeBool(node * n){
	if(n->var != NULL){
		return !strcmp(n->var->type,"Bool");
	}
	if(n->fun != NULL){
		if(call_check(n)>0)
			return 0;
		if(!strcmp(n->fun->returnn,"Bool"))
			return 1;
	}
    if(n->data != NULL){
	  	if(math_op(n)){
	  		//check_error(wrong_operator,n->line,0);
	  		return 0;
	  	}
	  	else if(rel_op(n) || parentesis(n)){
	  		if(!strcmp(n->data,"!") || parentesis(n))
	  			return checkNodeBool(n->child);
	  		else if(!strcmp(n->data,"=="))
	  			return checkNodeBool(n->child) && checkNodeBool(n->child->next) ||
	  				   checkNodeInt(n->child) && checkNodeInt(n->child->next);
	  		else if(!strcmp(n->data,"<") || !strcmp(n->data,">"))
	  			return checkNodeInt(n->child) && checkNodeInt(n->child->next);
	  		else	
	  			return checkNodeBool(n->child) && checkNodeBool(n->child->next);
	   	}
	    else if(!strcmp(n->data,"TRUE") || !strcmp(n->data,"FALSE"))
	      return 1;
	}
	return 0;
}


int call_check(node * aux){
	int nerrors = 0;
	node * call_params = aux->child->child;
	list * fun_params = aux->fun->params;
	if(!fun_params){
		if(call_params)
			nerrors += check_error(no_params_expected,aux->line,nerrors);
	}else{
		int err_found = 0;
		while(fun_params){
			if(!call_params){
				nerrors += check_error(wrong_number_of_params,aux->line,nerrors); err_found=1;
				break;
			}else{
				if(call_params->var){
					if(strcmp(fun_params->var->type,call_params->var->type)){
						nerrors += check_error(wrong_type_of_params,aux->line,nerrors); err_found=1;
						break;
					}
				}else{
					if(!strcmp(fun_params->var->type,"Integer") && !checkNodeInt(call_params)){
						nerrors += check_error(wrong_type_of_params,aux->line,nerrors); err_found=1;
						break;
					}
					if(!strcmp(fun_params->var->type,"Bool") && !checkNodeBool(call_params)){
						nerrors += check_error(wrong_type_of_params,aux->line,nerrors); err_found=1;
						break;
					}
				}

			}
			fun_params = fun_params->next;
			call_params = call_params->next;
		}
		
		if(err_found==0 && call_params)
			nerrors += check_error(wrong_number_of_params,aux->line,nerrors);
		
	}
	return nerrors;
}

int notInitializedVars(node * n){
	if(n->var != NULL){
		return !strcmp(n->var->value,"NULL");
	}
	if(n->fun != NULL){
		node * call_params = n->child->child;
		while(call_params){
			if(call_params->var && !strcmp(call_params->var->value,"NULL"))
				return 1;
			call_params=call_params->next;
		}
	}
  	else if(n->data != NULL){
		if(math_op(n) || rel_op(n) || parentesis(n)){
	  		if(!strcmp(n->data,"U-") || parentesis(n) || !strcmp(n->data,"!"))
	  			return notInitializedVars(n->child);
	  		else
	  			return notInitializedVars(n->child) || notInitializedVars(n->child->next);
	   	}
	    else
	    	return 0;
	}
	return 0;

}


int funCheck(node * nodo,char * ret){
	node * aux;
	int nerrors = 0;
	while(nodo){
		if(!strcmp(nodo->data,"=")){
			aux = nodo->child;
			if(!strcmp(aux->var->type,"Integer") && !checkNodeInt(aux->next))
				nerrors += check_error(asign_error,aux->line,nerrors);			
			else if(!strcmp(aux->var->type,"Bool") && !checkNodeBool(aux->next))
				nerrors += check_error(asign_error,aux->line,nerrors);
			if(notInitializedVars(aux->next))
				nerrors += check_error(not_initialized_vars,aux->line,nerrors);
			else
				aux->var->value="Inited";
		}else if(!strcmp(nodo->data,"if")){
			aux = nodo->child;
			if(checkNodeBool(aux)){
				if(aux->next){
					aux = aux->next;
					nerrors+=funCheck(aux->child,ret);
					if(aux->next){
						nerrors+=funCheck(aux->next->child,ret);
					}
				}
			}			
			else
				nerrors += check_error(condition_error,aux->line,nerrors);
		}else if(!strcmp(nodo->data,"while")){
			aux = nodo->child;
			if(checkNodeBool(aux)){
				if(aux->next)
					nerrors+=funCheck(aux->next,ret);
			}else
				nerrors += check_error(condition_error,aux->line,nerrors);
		}else if(!strcmp(nodo->data,"Return")){
			aux = nodo->child;
			if(!strcmp(ret,"Integer")){
				if(!checkNodeInt(aux))
					nerrors += check_error(expected_integer_return,aux->line,nerrors);
			}else if(!strcmp(ret,"Bool")){
				if(!checkNodeBool(aux))
					nerrors += check_error(expected_bool_return,aux->line,nerrors);
			}else if(!strcmp(ret,"VOID")){
				if(!(aux->data && !strcmp(aux->data,"VOID")))
					nerrors += check_error(expected_void_return,aux->line,nerrors);
			}else
				printf("Tipo de retorno desconocido\n");
		}else if(!strcmp(nodo->type,"function")){
			nerrors+=call_check(nodo);
		}

		nodo = nodo->next;
	}

	return nerrors;
}


int typeCheck(node * tree){
	node * reco = tree;
	reco = reco->child;
	int res=0; //cantidad de errores
	int ret=1; //1 si el chequeo de tipos es correcto, 0 si no lo es
	while(reco){
		if(debug)
			printf("Analizando funcion %s. Devuelve %s\n",reco->fun->name,reco->fun->returnn);
		res = funCheck(reco->child->child,reco->fun->returnn);
		if(res>0)
			ret=0;
		if(debug)
			printf("Se encontraron %d errores\n\n",res);
		reco = reco->next;
	}
	if(debug && ret == 1)
		printf("CHEQUEO DE TIPOS OK\n\n");
	return ret;
}

int check_all(stack * p,node * t){
	if(!exists_fun(p,"main")){
		printf("ERROR: no se encontro la funcion main\n");
		return 0;
	}
	return typeCheck(t);
}
