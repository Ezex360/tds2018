#ifndef DEFINITIONS_H_ 
#define DEFINITIONS_H_  



/* --- DECLARACION DEL ENUMERADO -- */
enum OPERATOR
{
    OP_INITF,OP_ENDF,OP_ASIGN,
    OP_SUMA,OP_RESTA,OP_DIV,OP_MULTI,
    OP_MOD,OP_MAY,OP_MEN,OP_EQ,OP_EXTERN,
    OP_AND,OP_OR,OP_URES,OP_NEG,OP_LOADP,
    OP_PAR,OP_IFFALSE,OP_JMP,OP_LABEL,
    OP_WHILE,OP_CALLF,OP_PUSHP,OP_RETURN,
    OP_GLOBAL_VAR,OP_VAR_INIC,OP_VAR_END

};
char * getNameOperator(enum OPERATOR x);

/* TABLA DE SIMBOLOS */
/* --- DECLARACION DE TIPOS  --- */
typedef struct list list;

/**
 * Estructura de variables
 */
typedef struct variable{
    char * type;
    char * name;
    char * value;
    int scope;
    int offset;
}variable;

/**
 * Estructura de funciones
 */
typedef struct function{
    char * returnn;
    char * name;
    int offset;
    list * params;
}function;

/**
 * Estructura de nodos
 */
typedef struct list{
    char * type;
    variable * var;
    function * fun;
    struct list * next;
}list;


/**
 * Pila de lista de variables
 */
typedef struct stack{
    int level;
    list * lista;
    struct stack * next; 
}stack;

/* --- DECLARACION DE FUNCIONES --- */
variable * new_var(char * tipo, char * nombre, char * valor,int scope, int offset);
stack * new_stack(int x);
int var_offset(stack * s);
int is_global(stack * s);   
void push();
void pop();
int add_var(variable * v);
int add_fun(function * f);
void init_stack();
void print_stack(stack * s);
void print_list(list * l);
int exists_var_list(list * l,char * n);
int exists_fun(stack *s,char * n);
function * new_fun(char * ret, char * nombre, list * l);
list * new_list(char * tipo, variable * var, function * fun);
variable * find_var(stack * s, char *n);
function * find_fun(stack * s, char *n);


/* ARBOL DE SINTAXIS */
/* --- DECLARACION DE TIPOS  --- */
/**
 * Estructura de Arbol N-Ario
 */
typedef struct nary_tree{
    enum OPERATOR op;
	char * type;
    char * data;
    int line;
    variable * var;
    function * fun;
	struct nary_tree * next; //hermanos
	struct nary_tree * child; //hijos
}node;

/* --- DECLARACION DE FUNCIONES --- */
node * new_node_char(char *);
node * new_node_char_op(char * data,enum OPERATOR op);
node * new_node_var(variable * data);
node * new_node_fun(function * data);
node * add_sibling(node *, node *);
node * add_child(node *, node *);
void remove_node(node* node);
void print_dfs(node * n);
void init_tree(char * data);


/* CODIGO INTERMEDIO */
/* --- DECLARACION DE TIPOS  --- */



/**
 * Estructura de Codigo de Tres Direcciones
 */
typedef struct instructions{
    enum OPERATOR op;
    variable * arg0;
    variable * arg1;
    variable * res;
    struct instructions * next;
}instruction;

/* --- DECLARACION DE FUNCIONES --- */

int check_all(stack *, node *);
instruction * inter_generator(stack * p,node * t);
void asm_generator(instruction * codigo);
char * new_label();
int is_operator(node * n);
node * optimize_tree(node * root);


#endif 


