%{

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "definitions.h"

int yylex();
int yyerror();
int add_f(char ret[],char name[],struct list * par);
int add_v(char * type,char * name,char * value,int scope, int offset);
void decl_local_vars(char * fname,list * params);
char * fname;
list * fparams;

char * var_type;
char * ret_type;
int max_offset=0;

extern node * root;
extern stack * pila;


%}
 
%union { int i; char *s; struct nary_tree * p; struct list * l; }
 
%token<s> PROGRAM
%token<s> BEGINN
%token<s> END
%token<s> VOID
%token<s> EXTERN
%token<s> BOOL
%token<s> IF
%token<s> ELSE
%token<s> THEN
%token<s> INTEGER
%token<s> RETURN
%token<s> WHILE
%token<s> TRUE
%token<s> FALSE
%token<i> INT
%token<s> ID
%token<s> AND
%token<s> OR
%token<s> EQUALS
%token<s> ERR_CHAR

%type<p> program
%type<p> list_method_decl
%type<l> params
%type<p> method_body
%type<p> method_decl
%type<p> block
%type<p> list_expr
%type<p> list_statement
%type<p> statement
%type<p> method_call
%type<p> expr
%type<p> type
%type<p> literal
%type<p> BOOLEAN
//%type<s> var_aux
//%type<s> list_ID
//%type<s> var_decl
//%type<s> method_return
//%type<s> bin_op
//%type<s> arith_op
//%type<s> rel_op
//%type<s> cond_op

%nonassoc '='
%left AND OR
%nonassoc '<' '>' EQUALS
%left '+' '-'
%left '*' '/' '%'
%left UNARY
 
%%

program: PROGRAM BEGINN END {root = new_node_char("program"); YYACCEPT;}
       | PROGRAM BEGINN var_decl END {root = new_node_char("program"); YYACCEPT;}
       | PROGRAM BEGINN list_method_decl END {
                        root = new_node_char("program");
                        add_child(root,$3);
                        YYACCEPT;}
       | PROGRAM BEGINN var_decl list_method_decl END {
                        root = new_node_char("program");
                        add_child(root,$4);
                        YYACCEPT;}
       ;

type: INTEGER {var_type = "Integer"; $$ = new_node_char("Integer");}
    | BOOL {var_type = "Bool"; $$ = new_node_char("Bool");}
    ;

list_ID: ID {max_offset = var_offset(pila);
             if(!add_v(var_type,$1,"NULL",is_global(pila),max_offset))
               YYABORT;}
       | list_ID ',' ID {max_offset = var_offset(pila);
                         if(!add_v(var_type,$3,"NULL",is_global(pila),max_offset))
                           YYABORT;}
       ;

var_decl: type list_ID ';' {}
        | var_decl type list_ID ';' {}
        ;

params: type ID {max_offset = var_offset(pila);
                 if(!add_v(var_type,$2,"Inited",0,max_offset))
                   YYABORT;
                 $$ = new_list("var",find_var(pila,$2),NULL);}       
       |type ID {max_offset = var_offset(pila);
                 if(!add_v(var_type,$2,"Inited",0,max_offset))
                  YYABORT;}
        ',' params { list * aux = new_list("var",find_var(pila,$2),NULL);
                     aux->next = $5;
                     $$ = aux;
                   } 
      ;

method_body: {push();} block {pop();} { $$ = $2;}
           | EXTERN ';' { $$ = new_node_char("Extern");}

method_decl: VOID ID '('  ')' {if(!add_f("VOID",$2,NULL)) YYABORT;} method_body { 
                              node * aux = new_node_fun(find_fun(pila,$2));
                              aux->fun->offset=max_offset;
                              max_offset=0;
                              node * body = new_node_char("code");
                              add_child(aux,body);
                              add_child(body,$6);
                              $$ = aux;}
           | type ID '('  ')' {if(!add_f(var_type,$2,NULL))YYABORT;} method_body { 
                              node * aux = new_node_fun(find_fun(pila,$2));
                              aux->fun->offset=max_offset;
                              max_offset=0;
                              node * body = new_node_char("code");
                              add_child(aux,body);
                              add_child(body,$6);
                              $$ = aux;}
           | VOID ID '('{push();} params ')' {if(!add_f("VOID",$2,$5))YYABORT;} method_body { 
                                    node * aux = new_node_fun(find_fun(pila,$2));
                                    aux->fun->offset=max_offset;
                                    max_offset=0;
                                    node * body = new_node_char("code");
                                    add_child(aux,body);
                                    add_child(body,$8);
                                    pop();
                                    $$ = aux;}
           | type ID '(' {ret_type=var_type;push();} params ')' {if(!add_f(ret_type,$2,$5))YYABORT;} method_body { 
                                                          node * aux = new_node_fun(find_fun(pila,$2));
                                                          aux->fun->offset=max_offset;
                                                          max_offset=0;
                                                          node * body = new_node_char("code");
                                                          add_child(aux,body);
                                                          add_child(body,$8);
                                                          pop();
                                                          $$ = aux;}
           ;

list_method_decl: method_decl { $$ = $1; }
                | list_method_decl method_decl { add_sibling($1,$2); }

block: BEGINN END {$$ = NULL;}
     | BEGINN var_decl END {$$ = NULL;} 
     | BEGINN list_statement END {$$ = $2;}
     | BEGINN var_decl list_statement END {$$ = $3;} 
     ; 

list_statement: statement {$$ = $1;}
              | list_statement statement {add_sibling($1,$2);}

statement: ID '=' expr ';' {node * asign=new_node_char("=");
                            variable * aux = find_var(pila,$1);
                            if(!aux){
                              yyerror("ERROR: asignacion a una variable no declarada\n");
                              YYABORT;
                            }
                            add_child(asign,new_node_var(aux));
                            add_child(asign,$3);
                            $$=asign;
                           }
         | method_call ';' {$$ = $1;}
         | IF '(' expr ')' THEN block {node *iff=new_node_char("if");
                                       add_child(iff,$3);
                                       node * if_true=new_node_char("if_true");
                                       add_child(if_true,$6);
                                       add_child(iff,if_true);
                                       $$=iff;
                                      }
         | IF '(' expr ')' THEN block ELSE block {node *iff=new_node_char("if");
                                                  add_child(iff,$3);
                                                  node * if_true=new_node_char("if_true");
                                                  add_child(if_true,$6);
                                                  add_child(iff,if_true);
                                                  node * if_false=new_node_char("if_false");
                                                  add_child(if_false,$8);
                                                  add_child(iff,if_false);
                                                  $$=iff;
                                                  }
         | WHILE expr block {node * whilee = new_node_char("while");
                             add_child(whilee,$2);
                             add_child(whilee,$3);
                             $$=whilee;
                            }
         | RETURN ';' {node * aux = new_node_char("Return"); 
                       add_child(aux,new_node_char("VOID"));
                       $$=aux;}
         | RETURN expr ';' {node * aux = new_node_char("Return");
                            add_child(aux,$2);
                            $$=aux;}
         | ';' {$$ = NULL;}
         | {push();} block {pop();}{$$ = $2;}
         ; 

list_expr: expr {$$ = $1;}
         | list_expr ',' expr {add_sibling($1,$3);}
         ;

method_call: ID '('  ')' { function * fun = find_fun(pila,$1);
                           if(!fun){
                              yyerror("ERROR: Funcion no declarada\n");
                              YYABORT;
                            }
                           node * aux = new_node_fun(fun);
                           add_child(aux,new_node_char("params"));
                           $$ = aux;
                         }
           | ID '(' list_expr ')' { function * fun = find_fun(pila,$1);
                                    if(!fun){
                                      yyerror("ERROR: Funcion no declarada\n");
                                      YYABORT;
                                    }
                                    node * aux = new_node_fun(fun);
                                    node * param = new_node_char("params");
                                    add_child(aux,param); 
                                    add_child(param,$3);
                                    $$ = aux;
                                  }
           ;

expr: ID {$$ = new_node_var(find_var(pila,$1));}
    | method_call {$$ = $1;}
    | literal {$$ = $1;}
    | expr '+' expr {node * op = new_node_char_op("+",OP_SUMA);  add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr '-' expr {node * op = new_node_char_op("-",OP_RESTA); add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr '*' expr {node * op = new_node_char_op("*",OP_MULTI); add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr '/' expr {node * op = new_node_char_op("/",OP_DIV);   add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr '<' expr {node * op = new_node_char_op("<",OP_MEN);   add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr '>' expr {node * op = new_node_char_op(">",OP_MAY);   add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr '%' expr {node * op = new_node_char_op("%",OP_MOD);   add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr AND expr {node * op = new_node_char_op("AND",OP_AND); add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr OR  expr {node * op = new_node_char_op("OR",OP_OR);   add_child(op,$1);add_child(op,$3);$$ = op;}
    | expr EQUALS expr {node * op = new_node_char_op("==",OP_EQ);add_child(op,$1);add_child(op,$3);$$ = op;}
    | '-' expr %prec UNARY {node * op = new_node_char_op("U-",OP_URES);add_child(op,$2);$$ = op;}
    | '!' expr %prec UNARY {node * op = new_node_char_op("!",OP_NEG);add_child(op,$2);$$ = op;}
    | '(' expr ')' {node * op = new_node_char_op("()",OP_PAR);   add_child(op,$2);$$ = op;}
    ;

literal: INT { char * str = malloc(sizeof(char[16]));
               sprintf(str, "%d", $1);
               $$ = new_node_char(str);
             }
       | BOOLEAN {$$ = $1;}
       ;

BOOLEAN: TRUE {$$ = new_node_char("TRUE");}
       | FALSE {$$ = new_node_char("FALSE");}
       ; 

/*
bin_op: arith_op {}
      | rel_op {}
      | cond_op {}
      ;

arith_op: '+' {}
        | '-' {}
        | '*' {}
        | '/' {}
        | '%' {}
        ;

rel_op: '<' {}
      | '>' {}
      | EQUALS {}
      ;

cond_op: AND {}
       | OR {}
       ;
*/


%%

int add_f(char * ret,char * name,struct list * par){
  if(!add_fun(new_fun(ret,name,par))){
    char prin[64] = "ERROR: Funcion ";
    strcat(prin,name);
    strcat(prin, " ya declarada.");
    yyerror(prin);
    return 0;
  }
  return 1;
}

int add_v(char * type,char * name,char * value,int scope, int offset){
  if(!add_var(new_var(type,name,value,scope,offset))){
    char prin[64] = "ERROR: Variable ";
    strcat(prin,name);
    strcat(prin, " ya declarada.");
    yyerror(prin);
    return 0;
  }
  return 1;
}

