#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "definitions.h"

stack * pila;
void print_stack(stack * s);
int exists_lista(list * l,char * n);
int yyerror();

/* --- IMPLEMENTACION DE FUNCIONES --- */
variable * new_var(char * tipo, char * nombre, char * valor,int scope, int offset){
    variable * aux = (variable *) malloc(sizeof(variable));
    aux->type = tipo;
    aux->name = nombre;
    aux->value = valor;
    aux->scope = scope;
    aux->offset = offset;
    return aux;
}

function * new_fun(char * ret, char * nombre, list * l){
    function * aux = (function *) malloc(sizeof(function));
    aux->returnn = ret;
    aux->name = nombre;
    aux->params = l;
    return aux;
}

list * new_list(char * tipo, variable * var, function * fun){
    list * aux = (list *) malloc(sizeof(list));
    aux->type = tipo;
    aux->var = var;
    aux->fun = fun;
    aux->next = NULL;
    return aux;
}

stack * new_stack(int x){
    stack * s = (stack *) malloc(sizeof(stack));
    s->level = x;
    s->lista = NULL;
    s->next = NULL;
}

void push(){
    //printf("push\n");
    stack * aux = new_stack(pila->level+1);
    aux->next = pila;
    pila = aux;
}

void pop(){
    //printf("pop\n");
    pila = pila->next;
}

void init_stack(){
    pila = new_stack(0);
}

void print_list(list * l){
    list * reco = l;
    while(reco){
        if(reco->var)
            printf("\nvariable %s %s\n",reco->var->type,reco->var->name);
        if(reco->fun){
            printf("\nfuncion %s %s\n",reco->fun->name,reco->fun->returnn);
            if(reco->fun->params){
                printf("params: ");
                print_list(reco->fun->params);
            }
        }
        reco = reco->next;
    }
}

void print_stack(stack * s){
    if(s==NULL)
        printf("VACIO\n");
    else{
        printf("nivel: %d",s->level );
        print_list(s->lista);
        if(s->next)
            print_stack(s->next);
    }
    
}

int add_var(variable * v){
    if(exists_lista(pila->lista,v->name)){
        return 0;
    }else{
        list * l = (list *) malloc(sizeof(list));
        l->var = v;
        if(pila->lista){
            l->next = pila->lista;
            pila->lista =  l;
        }else{
            l->next = NULL;
            pila->lista = l;
        }
    }
    return 1;
    //printf("Agregue variable %s %s %s\n",v->type,v->name,v->value );
}

int add_fun(function * f){
    stack * reco=pila; 
    if(exists_fun(pila,f->name)){
       return 0;
    }else{
        list * l = (list *) malloc(sizeof(list));
        l->fun = f;
        while(reco->next)
            reco=reco->next;
        if(reco->lista){
            l->next = reco->lista;
            reco->lista = l;
        }else{
            l->next = NULL;
            reco->lista = l;
        }
    }
    return 1;
    //printf("Agregue Funcion %s %s\n",f->returnn,f->name);
}

int exists_lista(list * l,char * n){
    list * reco = l;
    while(reco){
        if(reco->var && strcmp(reco->var->name,n) == 0)
            return 1;
        reco = reco->next;
    }
    return 0;
}

int exists_fun(stack *s,char * n){
    stack * head = s;
    while(head->next)
        head = head->next;
    list * reco = head->lista;
    if(reco != NULL)
    while(reco){
        if(reco->fun && strcmp(reco->fun->name,n) == 0){
            return 1;
        }
        reco = reco->next;
    }

    return 0;
}

int exists_var(stack * s,char * n){
    stack * reco = s;
    while(reco){
        if(exists_lista(reco->lista,n))
            return 1;
        reco = reco->next;
    }
    return 0;
}

variable * find_var(stack * s, char *n){
    stack * head = s;
    list * reco;
    while(head){
        reco = head->lista;
        while(reco){
            if(reco->var && strcmp(reco->var->name,n) == 0)
                return reco->var;
            reco = reco->next;
        }
        head = head->next;
    }

    return NULL;
}

function * find_fun(stack * s, char *n){
    stack * head = s;
    list * reco;
    while(head){
        reco = head->lista;
        while(reco){
            if(reco->fun && strcmp(reco->fun->name,n) == 0)
                return reco->fun;
            reco = reco->next;
        }
        head = head->next;
    }

    return NULL;
}


int is_global(stack * s){
    return s->level == 0;
}

int cant_var_local(stack * s){
    stack * aux = s;
    list * reco;
    int n_var=0;
    while(aux->next){
        reco=aux->lista;
        while(reco){
            if(reco->var)
                n_var++;
            reco=reco->next;
        }
        aux=aux->next;
    }
    return n_var;
}

int var_offset(stack * s){
    return (cant_var_local(s)+1)*8;
}

/*

int main(int argc, char const *argv[])
{   
    printf("inicializo\n");
    init_stack();

    function * aux = new_fun("int","uno",NULL);
    printf("\n");
    add_fun(aux);
    print_stack(pila);


    return 0;


}

    print_stack(pila);

    printf("\n_-_-_-_-_\n\n");

    printf("agrego variable\n");
    variable * aux = new_var("int","x","NULL");
    add_var(aux);
    add_var(aux);
    add_var(aux);
    add_var(aux);
    print_stack(pila);
    printf("\n_-_-_-_-_\n\n");
    push();
    variable * aux1 = new_var("int","x1","NULL");
    add_var(aux1);
    add_var(aux1);
    print_stack(pila);
    pop();
    printf("\n_-_-_-_-_\n\n");
    if(exists_var(pila,"x"))
        printf("existe x\n");
    else
        printf("no existe x\n");
*/