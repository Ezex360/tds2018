/*
  Pre-Proyecto Taller de Diseño de Software
  Integrantes:
    Fischer, Sebastian 37128158
    Gardiola, Joaquin 38418091
    Giachero, Ezequiel 39737931
*/

%{

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "definitions.h"


#include "calc-sintaxis.tab.h"

void yyerror(char *msg);
void fatal_error();
int comment_nesting = 0;

int debug=0,codinter=0,opt=0;
char *filename;

extern node * root;
extern stack * pila;

extern void print_ins(instruction *);

%}

%x COMMENT

%option noyywrap
%option yylineno

letra [a-zA-z]
digito [0-9]
arith_op [+-/%*]
rel_op [<>]

OPEN "{"
CLOSE "}"

%%

"program"                     { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("program : %s\n",yytext);
                                return PROGRAM;}

"begin"                       { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("begin : %s\n",yytext);
                                return BEGINN;}

"end"                         { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("end : %s\n",yytext);
                                return END;}

"void"                        { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("void : %s\n",yytext);
                                return VOID;}

"extern"                      { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("extern : %s\n",yytext);
                                return EXTERN;}

"bool"                        { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("bool : %s\n",yytext);
                                return BOOL;}

"if"                          { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("if : %s\n",yytext);
                                return IF;}

"else"                        { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("else : %s\n",yytext);
                                return ELSE;}

"then"                         { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("then : %s\n",yytext);
                                return THEN;}

"integer"                      { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("integer : %s\n",yytext);
                                return INTEGER;}

"return"                      { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("return : %s\n",yytext);
                                return RETURN;}

"while"                       { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("while : %s\n",yytext);
                                return WHILE;}

"true"                         { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("true : %s\n",yytext);
                                return TRUE;}

"false"                         { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("false : %s\n",yytext);
                                return FALSE;}


{digito}+                     { yylval.i = atoi(yytext);
                                //printf("INT : %d\n",atoi(yytext)); 
                                return INT;}

{letra}({letra}|{digito})*    { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("ID : %s\n",yytext);
                                return ID;}

{arith_op}|{rel_op}|"="|"!"   { //printf("OPERADOR: %s\n",yytext);
                                return *yytext;}

";"|","|"("|")"               { //printf("COMAS: %s\n",yytext);
                                return *yytext;}

"&&"                          { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("AND: %s\n",yytext);
                                return AND;}

"||"                          { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("OR: %s\n",yytext);
                                return OR;}

"=="                          { yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
                                strcpy(yylval.s,yytext);
                                //printf("EQUALS: %s\n",yytext);
                                return EQUALS;}

"//".*\n                      {//printf("Comentario: %s\n",yytext);
                              }

"{"                           {BEGIN(COMMENT); ++comment_nesting;}
<COMMENT>{OPEN}                 {++comment_nesting;}
<COMMENT>[^({CLOSE})({OPEN})(EOF)] {}
<COMMENT>{CLOSE}                {if (--comment_nesting == 0) BEGIN(INITIAL);}
<COMMENT>(EOF)                {yyerror("Comment reach end of the text");fatal_error();}


[ \t\n]+|\n                   ;//Ignore whitespace and lineskip

.                             yyerror("Invalid character");



%%


void yyerror(char *msg){
	printf("%sLine: %d\n",msg,yylineno);
}

void fatal_error(){
    printf("No se puede seguir con la interpretacion\n");
    exit(1);
}

int main(int argc,char *argv[]){
	++argv,--argc;
	if (argc > 0){
		yyin = fopen(argv[0],"r");
        filename = argv[0];
    }
	else
		yyin = stdin;
    if(argc > 1 && !strcmp(argv[1],"debug"))
        debug = 1;
    if(argc > 2 && !strcmp(argv[2],"codinter"))
        codinter = 1;
    if(argc > 3 && !strcmp(argv[3],"optimize"))
        opt = 1;


    init_stack();
	if(yyparse())
        fatal_error();
    if(debug){
        print_dfs(root);
        printf("\n\n");
        print_stack(pila);
        printf("\n");
    }
    if(!check_all(pila,root))
        fatal_error();
    if(opt)
        root = optimize_tree(root);
    if(opt && debug){
        print_dfs(root);
        printf("\n\n");
    }
    instruction * code = inter_generator(pila,root);
    if(debug || codinter)
        print_ins(code);
    if(!codinter) //hasta codigo intermedio
        asm_generator(code);

}
