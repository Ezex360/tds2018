void printGuiones( );
void print(int n);


//Muestra los primenos "n" numeros de la serie de Fibonacci. 
int fibonacci(int n)
{
  int i, term1, term2, nextTerm;
  term1 = 0;
  term2 = 1;
  i = 1;
  while (i < n)
    {
      nextTerm = term1 + term2;
      term1 = term2;
      term2 = nextTerm;
      i = i+1;
    }
  return term2;
}

// potencia de  x^n
int potencia (int x, int n)
{
  int i,r;
  i = 1;
  r = x;
  while (i<n)
  	{
  		r = r*x;
  		i = i+1;
  	}
  return r;
}

// retorna el factorial de v
int factorial (int v)
{
	int limit,c,fact;
	limit = 15;
	if ( v > limit) 
  	{ 
    	return(-1);
  	}
	else 
  	{ 
    	c = 0;
    	fact = 1;
    	while (c<v)
      	{
        	c = c+1;
        	fact = fact*c;
      	}
    	return fact;
  	}
}

// retorna el n esimo primo
int nthprime (int n) 
{
  int i, divs;
  int divides;
  i = 0;
  divs = 2;
  n = n+1;
  while ( n > 0) 
    {
      divides = 0; 
      i = i + 1;
      while ( ! divides && divs < i) 
        {
          if ((i%divs) == 0) 
            {
              divides = 1;
            }
          else
            {
              divs = divs+1;
            }
        }
      divs = 2;
      if (!divides) 
        {
          n = n-1;
        }
    }
  return i;
}

// retorna el maximo comun divisor de a y b,
int gcd (int a, int b) 
{
  int i, result;
  i = 1;
  result = i;
  while ( i < (a+b)) 
    {
      if (((a % i) == 0) &&  ((b%i) ==0 )) 
        {
          result = i;
        }
      i = i+1;
    }
  return result;
}

void test()
{
  int aux, result;
  result = gcd(factorial(3),factorial(4)); //gcd(6,24) = 6
  print( result);
  aux = nthprime(gcd(factorial(3),factorial(4))); //nthprime(6) = 13
  print( aux);
  aux = potencia(3,nthprime(gcd(factorial(3),factorial(4)))); //potencia(3,13) = 1594323
  print( aux);
}

// invoca test
void test1()
{
  int aux;
  aux = 2; 
  test();
  print(aux);
}

int main()
{
  int x, i, aux, res;
  // test fibonacci 
  printGuiones(); // printf("-----------")
  x=5; //cantidad de veces que ejecutara la funcion
  i = 0;
  aux=1; // valor para invocar a la funcion
  while (i<x)
    {
      res = fibonacci(aux);
      aux = aux + 1;
      print( res);
      i=i+1;
    }      
  // test factorial entero
  printGuiones(); // printf("-----------")
  x=4; // cantidad de veces que ejecutara la funcion
  i = 0;
  aux=3; // dato para invocar a la funcion
  while (i<x)
    {
      res = factorial(aux);
      aux = aux + 1;
      print( res);
      i=i+1;
    }
  // test nthprime entero
  printGuiones(); // printf("-----------")
  x=10; // cantidad de veces que ejecutara la funcion
  i = 0;
  aux = 4;
  while (i<x)
    {
      res = nthprime(aux);
      aux = aux + 1;
      print( res);
      i=i+1;
    }
  // test gcd entero    
  printGuiones(); // printf("-----------")
  x=3; // cantidad de veces que ejecutara la funcion
  i = 0;
  while (i<x)
    { 
      print( gcd(8+i,2+i));
      i=i+1;
    }
  // test potencia entero
  printGuiones(); // printf("-----------")
  x=3; // cantidad de veces que ejecutara la funcion
  i = 0;
  while (i<x)
    {
      print( potencia(2+i,1+i) + 1);
      i=i+1;
    }
  // test test
  printGuiones(); // printf("-----------")
  test();
  // test test1 
  printGuiones(); // printf("-----------")
  test1();
  return 1;
}